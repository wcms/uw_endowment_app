# UW Endowment Application #

This project is migration of legacy PHP application previously hosted on servers that are scheduled for retirement. Before this was standalone application and this project is about migration to Drupal 7 system.

### Requirements ###

+ Drupal 7 (up to date core recommended)
+ Module requirements (version used in development/release):
    * [Views](https://www.drupal.org/project/views) (7.x-3.18)
    * [CAS](https://www.drupal.org/project/cas) (7.x-1.7)
    * [Real Name](https://www.drupal.org/project/realname) (7.x-1.3)
    * [Views Data Export](https://www.drupal.org/project/views_data_export) (7.x-3.2)
    * [Date](https://www.drupal.org/project/date) (7.x-2.10)
    * [uw_php_apps_api](https://git.uwaterloo.ca/wcms/uw_php_apps_api) (7.x-1.2) - uw custom module

### How do I get set up? ###

* Place this module on discoverable path (sites/all/module/custom for example) 
* Use Web UI or Drush to enable it. All configuration comes preset.

### Module details ###
#### Module configuration ####
 **/admin/endowment**
 
  This is main admin configuration page for the module, all module's pages (configuration and data) have this page as parent. Check tab menus for access to submissions, students, export and configuration specific pages.
  
#### Role and Permissions ####
* Role: **UW Endowment Administrator**
    
     Allows access to submissions and allows configuration changes to this module.

### Documentation ###
Check here for [documentation about module and usage](https://docs.google.com/document/d/1XvzaFZvS3pcfAu7U3le0GkkheJz9c4w3tEMeh0RP79w/edit?usp=sharing).
