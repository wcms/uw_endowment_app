<?php

/**
 * @file
 *
 * Endowment App admin forms, content
 */

/**
 * Admin configuration form
 *
 * Custom settings can be found here. Messages that are displayed are also
 * found here. Scheduler options and submission form state.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function uw_endowment_app_admin_form($form, &$form_state) {
  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Request form configuration'),
  ];

  $form['group1']['uw_endowment_app_submissions_allowed'] = [
    '#type' => 'select',
    '#title' => t('Current state'),
    '#options' => [0 => t('Closed'), 1 => t('Open')],
    '#default_value' => variable_get('uw_endowment_app_submissions_allowed'),
    '#description' => t('Set request form state manually and effective right away. Open means students can submit their requests. While closed prevents new submissions.'),
  ];

  $form['group1']['uw_endowment_app_max_requests'] = [
    '#type' => 'select',
    '#title' => t('Max refund requests per student'),
    '#options' => drupal_map_assoc(range(1, 5)),
    '#default_value' => variable_get('uw_endowment_app_max_requests', 3),
  ];

  $form['group1']['uw_endowment_app_current_term'] = [
    '#type' => 'textfield',
    '#title' => t('Current term code'),
    '#description' => t('This will be attached to every request as a way to determine which term request belongs to.'),
    '#default_value' => variable_get('uw_endowment_app_current_term'),
    '#size' => 10,
    '#required' => TRUE,
  ];

  $form['group1']['group1_1'] = [
    '#type' => 'fieldset',
  ];

  $form['group1']['group1_1']['uw_endowment_app_schedule_state'] = [
    '#type' => 'checkbox',
    '#title' => t('Schedule state change'),
    '#description' => t('Set state change to occur on specified date and time. New state will be changed on first cron run after specified date and time.'),
    '#default_value' => variable_get('uw_endowment_app_schedule_state', FALSE),
  ];

  $form['group1']['group1_1']['uw_endowment_app_schedule_new_status'] = [
    '#type' => 'select',
    '#title' => t('<strong>Change state to</strong>'),
    '#options' => [0 => t('Closed'), 1 => t('Open')],
    '#default_value' => variable_get('uw_endowment_app_schedule_new_status'),
    '#description' => t('Select new state to be triggered on specified date and time'),
    '#states' => [
      'visible' => [
        ':input[name="uw_endowment_app_schedule_state"]' => ['checked' => TRUE],
      ],
    ],
  ];

  $form['group1']['group1_1']['uw_endowment_app_schedule_new_status_date'] = [
    '#type' => 'date_popup',
    '#title' => t('Change state on'),
    '#default_value' => variable_get('uw_endowment_app_schedule_new_status_date'),
    '#states' => [
      'visible' => [
        ':input[name="uw_endowment_app_schedule_state"]' => ['checked' => TRUE],
      ],
    ],
  ];

  $form['group3'] = [
    '#type' => 'fieldset',
    '#title' => t('Admin mode'),
    '#collapsible' => TRUE,
    '#collapsed' => !variable_get('uw_endowment_app_admin_mode'),
  ];

  $form['group3']['uw_endowment_app_admin_mode'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable admin mode'),
    '#description' => t('This will enable you to make submission as any student, it will use data from student database to mimic student.'),
    '#default_value' => variable_get('uw_endowment_app_admin_mode'),
  ];

  $override_suffix = '';
  if ($email_override = variable_get('uw_endowment_app_reroute_email')) {
    $override_suffix = t('(Email override detected: <strong>@email</strong>)', ['@email' => check_plain($email_override)]);
  }

  $form['group3']['uw_endowment_app_send_email_receipt'] = [
    '#type' => 'checkbox',
    '#title' => t('Send email receipt !override', ['!override' => $override_suffix]),
    '#description' => t('Option to send email notification / receipt to person requesting endowment. Use $config["uw_endowment_app_reroute_email"] in settings.php file to reroute all emails.'),
    '#default_value' => variable_get('uw_endowment_app_send_email_receipt'),
  ];

  return system_settings_form($form);
}


/**
 * Admin form validation
 *
 * @param $form
 * @param $form_state
 */
function uw_endowment_app_admin_form_validate($form, &$form_state) {
  global $user;

  $values = $form_state['values'];
  $term = $values['uw_endowment_app_current_term'];

  $stored_term = variable_get('uw_endowment_app_current_term');

  if ($stored_term && !empty($stored_term) && $stored_term !== $term) {
    // Clear term cache in case term was updated.
    cache_clear_all("uw_endowment_app:refunds:term", 'cache');
  }

  module_load_include('inc', 'uw_php_apps_api', 'includes/uw_php_apps_api.functions');

  $casusers = [$user->uid => $user];
  cas_user_load($casusers);
  $cas_id = $casusers[$user->uid]->cas_name;

  if ($cas_id) {
    $uwuser = uw_php_apps_api_user_data($cas_id);

    if (!$uwuser) {
      form_set_error('uw_endowment_app_student_endpoint', t('API test failed for current user.'));
    }
    else {
      drupal_set_message(t('API endpoint tested successfully for current user.'));
    }
  }
  else {
    drupal_set_message(t('Unable to test API endpoint for current user. User record is missing CAS username.'), 'warning');
  }
}

/**
 * Custom form messages
 *
 * You can configure messages that are displayed to students, informing them
 * that form has been closed for new submission or that they had already
 * submitted but they are not allowed to update their submission.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function uw_endowment_app_messages_form($form, &$form_state) {
  $form['group0'] = [
    '#type' => 'fieldset',
    '#title' => t('Form help'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['group0']['uw_endowment_app_message_help'] = [
    '#type' => 'text_format',
    '#title' => t('Help text'),
    '#format' => variable_get('uw_endowment_app_message_help')['format'],
    '#default_value' => variable_get('uw_endowment_app_message_help')['value'],
    '#description' => t('Content that will be displayed below the title and above form.'),
    '#rows' => 20,
  ];

  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Form'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['group1']['uw_endowment_app_message_intro'] = [
    '#type' => 'text_format',
    '#title' => t('Intro message (text above the form)'),
    '#format' => variable_get('uw_endowment_app_message_intro')['format'],
    '#default_value' => variable_get('uw_endowment_app_message_intro')['value'],
    '#rows' => 20,
  ];

  $form['group2'] = [
    '#type' => 'fieldset',
    '#title' => t('Form closed'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['group2']['uw_endowment_app_message_form_closed'] = [
    '#type' => 'text_format',
    '#title' => t('Form closed message'),
    '#format' => variable_get('uw_endowment_app_message_form_closed')['format'],
    '#default_value' => variable_get('uw_endowment_app_message_form_closed')['value'],
    '#rows' => 20,
  ];

  $form['group3'] = [
    '#type' => 'fieldset',
    '#title' => t('Submission update'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['group3']['uw_endowment_app_message_resubmission'] = [
    '#type' => 'text_format',
    '#title' => t('Update not allowed message'),
    '#format' => variable_get('uw_endowment_app_message_resubmission')['format'],
    '#default_value' => variable_get('uw_endowment_app_message_resubmission')['value'],
    '#rows' => 20,
  ];

  $form['group4'] = [
    '#type' => 'fieldset',
    '#title' => t('Error messages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['group4']['uw_endowment_app_message_error_max_req'] = [
    '#type' => 'textfield',
    '#title' => t('Request number error'),
    '#default_value' => variable_get('uw_endowment_app_message_error_max_req'),
    '#description' => t('Error message if number of refund requests is over @refunds. Use plain text (no HTML). You can use @maxrefunds placeholder that will be replaced with actual maximum number of requests.', ['@refunds' => variable_get('uw_endowment_app_max_requests', 3)]),
    '#size' => 80,
    '#required' => TRUE,
  ];

  $form['group4']['uw_endowment_app_message_error_missing_student_number'] = [
    '#type' => 'textfield',
    '#title' => t('Missing Student Number'),
    '#default_value' => variable_get('uw_endowment_app_message_error_missing_student_number'),
    '#description' => t('Student record (API result) is missing student number.'),
    '#size' => 80,
    '#required' => TRUE,
  ];

  $form['group4']['uw_endowment_app_message_error_missing_student_user_id'] = [
    '#type' => 'textfield',
    '#title' => t('Missing Student User Id'),
    '#default_value' => variable_get('uw_endowment_app_message_error_missing_student_user_id'),
    '#description' => t('Student record (API result) is missing student user id.'),
    '#size' => 80,
    '#required' => TRUE,
  ];

  $form['group4']['uw_endowment_app_message_error_submission_exists'] = [
    '#type' => 'textfield',
    '#title' => t('Sumission exists'),
    '#default_value' => variable_get('uw_endowment_app_message_error_submission_exists'),
    '#description' => t('Student record (API result) is missing student user id.'),
    '#size' => 80,
    '#required' => TRUE,
  ];

  return system_settings_form($form);
}

/**
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function uw_endowment_app_refund_adv_form($form, &$form_state) {
  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Request type mapping'),
    '#collapsible' => TRUE,
  ];

  $form['group1']['uw_endowment_app_refund_types'] = [
    '#type' => 'textarea',
    '#title' => t('Refund types'),
    '#description' => t("One key value per line, format key|value. Order is important also. Example value: AHS|Applied Health Sciences Endowment Fund (27.44). Leave empty for default values, this can be used to get valid format."),
    '#default_value' => variable_get('uw_endowment_app_refund_types'),
    '#rows' => 12,
    '#states' => [
      'visible' => [
        ':input[name="uw_endowment_app_request_type"]' => [
          'value' => 'simple',
        ],
      ],
    ],
  ];

  return system_settings_form($form);
}

/**
 * Validation hook for experimental advanced form
 *
 * @param $form
 * @param $form_state
 */
function uw_endowment_app_refund_adv_form_validate($form, &$form_state) {
  if ($form_state['values']) {
    if (empty($form_state['values']['uw_endowment_app_refund_types'])) {
      $form_state['values']['uw_endowment_app_refund_types'] = UW_ENDOWMENT_APP_DEFAULT_REFUNDS_TYPES;
    }
    elseif (strip_tags($form_state['values']['uw_endowment_app_refund_types']) != $form_state['values']['uw_endowment_app_refund_types']) {
      form_set_error('uw_endowment_app_refund_types', t('The <em>Refund types</em> field must not contain HTML. Make sure not to use HTML markup for this field. Use plain text.'));
    }
  }
}

/**
 * Called from view this is used to enable resubmission update
 *
 * @param $sid
 *    Number of submission (this is used as primary key of the submission table)
 */
function uw_endowment_app_update_submission_status($sid) {
  module_load_include('inc', 'uw_endowment_app', 'includes/uw_endowment_app.submission');
  $updated = _uw_endowment_app_submission_allow_update_toggle($sid);

  if ($updated === '1') {
    drupal_set_message(t('Record successfully updated.'));
  }

  drupal_goto('admin/endowment/data');
}

/**
 * Email template and test form
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function uw_endowment_app_email_templates_form($form, &$form_state) {
  global $user;

  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Email Template Test'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['group1']['test_receipt'] = [
    '#type' => 'checkbox',
    '#title' => t('Send test email'),
    '#description' => t('Enter destination email address below to see how email receipt looks like.'),
    '#default_value' => FALSE,
  ];

  $form['group1']['test_email_address'] = [
    '#type' => 'textfield',
    '#title' => t('Destination email address'),
    '#description' => t('Make sure to enter valid email address and once you click "Send test email" notification will be send.'),
    '#states' => [
      'required' => [
        ':input[name="test_receipt"]' => array('checked' => TRUE),
      ],
      'visible' => [
        ':input[name="test_receipt"]' => array('checked' => TRUE),
      ],
    ],
    '#default_value' => $user->mail,
  ];

  $form['group1']['test_receipt_button'] = [
    '#type' => 'submit',
    '#value' => t('Send test email'),
    '#states' => [
      'enabled' => [
        ':input[name="test_email_address"]' => array('filled' => TRUE),
      ],
      'visible' => [
        ':input[name="test_receipt"]' => array('checked' => TRUE),
      ],
    ],
    '#submit' => ['test_receipt_button_submit'],
  ];

  $form['group2'] = [
    '#type' => 'fieldset',
    '#title' => t('Email Receipt Template'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['group2']['uw_endowment_app_email_receipt_subject'] = [
    '#type' => 'textfield',
    '#title' => t('Subject line'),
    '#description' => t('Email\'s subject line. Placeholders: @receipt_no'),
    '#default_value' => variable_get('uw_endowment_app_email_receipt_subject', 'Endowment Email Receipt: @receipt_no'),
    '#required' => TRUE,
  ];

  $form['group2']['uw_endowment_app_email_receipt_body'] = [
    '#type' => 'textarea',
    '#title' => t('Receipt Email Template'),
    '#description' => t('Email needs to be text only, available placeholders: @receipt_no, @student_user_id, @student_email, @student_number, @student_name, @refund, @term, @date'),
    '#default_value' => variable_get('uw_endowment_app_email_receipt_body'),
    '#rows' => 30,
    '#required' => TRUE,
  ];

  return system_settings_form($form);
}

/**
 * Test receipt submit handler
 *
 * @param $form
 * @param $form_state
 */
function test_receipt_button_submit($form, &$form_state) {
  $placeholders = [
    'receipt_no' => '123456',
    'student_user_id' => 'studentid123',
    'student_email' => 'studentid123@uwaterloo.ca',
    'student_name' => 'John Doe',
    'refund' => 'AHS, WEET, WEET_SC',
    'term' => '1185',
    'date' => format_date(REQUEST_TIME, 'custom', 'm/d/Y'),
    'student_number' => '12345678'
  ];

  $to = $form_state['values']['test_email_address'];

  module_load_include('inc', 'uw_endowment_app', 'includes/uw_endowment_app.email');
  $result = uw_endowment_app_email_send_receipt_notification($to, $placeholders);

  if ($result) {
    drupal_set_message(t('Test email has been send. Check your inbox(spam) folder.'));
  }
  else {
    drupal_set_message(t('There was an issue with sending email.'), 'error');
  }
}
