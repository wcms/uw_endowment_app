<?php

/**
 * @file
 *
 * UW Endowment constants file
 */

define('UW_ENDOWMENT_APP_SUBMISSIONS_OPEN', FALSE);
define('UW_ENDOWMENT_APP_SUBMISSIONS_TABLE', 'uw_endowment_app_submissions');
define('UW_ENDOWMENT_APP_SUBMISSIONS_REFUND_TABLE', 'uw_endowment_app_submissions_refunds');

define('UW_ENDOWMENT_APP_DEFAULT_REFUNDS_TYPES', "AHS|Applied Health Sciences Endowment Fund ($27.44)\r\nACCTG|Accountancy Endowment Fund ($20.00)\r\nARTS|Arts Endowment Fund ($12.00)\r\nWEEF|Engineering Endowment Fund (except Software Engineering) ($75.00)\r\nWEEF_SE|Engineering Endowment Fund (Software Engineering) ($26.60)\r\nENV|Environment Endowment Fund ($30.00)\r\nGSEF|Graduate Studies Endowment Fund ($20.00)\r\nMATH|Mathematics Endowment Fund (except Software Engineering) ($31.42)\r\nMATH_SE|Mathematics Endowment Fund (Software Engineering) ($26.60)\r\nSCI|Science Endowment Fund ($50.00)\r\nSFD|Science Foundation ($3.00)\r\n");
