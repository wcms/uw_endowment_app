<?php
/**
 * @file
 *
 * Email handler for uw_endowment_app module
 */


function uw_endowment_app_email_send_receipt_notification($to, $values) {
  global $user;
  $user_lang = user_preferred_language($user);

  $placeholders = [
    '@receipt_no' => $values['receipt_no'],
    '@student_user_id' => $values['student_user_id'],
    '@student_email' => $values['student_email'],
    '@student_name' => $values['student_name'],
    '@refunds' => $values['refund'],
    '@term' => $values['term'],
    '@date' => format_date(REQUEST_TIME, 'custom', 'm/d/Y'),
    '@student_number' => $values['student_number'],
  ];

  $destination = variable_get('uw_endowment_app_reroute_email', $to);

  $email_subject = variable_get('uw_endowment_app_email_receipt_subject');
  $email_body = variable_get('uw_endowment_app_email_receipt_body');

  $result = drupal_mail(
    'uw_endowment_app',
    'uw_endowment_app_email_receipt',
    $destination,
    $user_lang->language,
    [
      'subject' => $email_subject,
      'body' => $email_body,
      'replacements' => $placeholders,
    ]
  );

  return isset($result) && isset($result['result']) ? $result['result'] : FALSE;
}