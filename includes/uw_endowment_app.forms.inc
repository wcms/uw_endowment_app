<?php

/**
 * @file
 *
 * UW Endowment module form
 */

/**
 * Endowment request form
 *
 * @param $form
 *    Form itself
 * @param $form_state
 *    State of form, ajax is used to populate form fields
 *
 * @return mixed
 *    Form array
 */
function uw_endowment_app_request_form($form, &$form_state) {
  if (user_is_anonymous()) {
    drupal_set_message(t('Please login. Anonymous users can\'t submit request.'), 'warning');
    drupal_goto('user', ['query' => ['destination' => current_path()]]);
  }

  $admin_access = variable_get('uw_endowment_app_admin_mode') && user_access('administer uwaterloo endowment app');
  $submissions_allowed = variable_get('uw_endowment_app_submissions_allowed');

  if (!$submissions_allowed && !$admin_access) {
    return _uw_endowment_app_request_form_closed();
  }

  // This is needed to allow default_value of the form field to be used.
  // Default value is used only when input array is empty
  if ($admin_access && !empty($form_state['input'])) {
    foreach ($form_state['input'] as $key => $item) {
      unset($form_state['input'][$key]);
    }
  }

  if ($admin_access) {
    $form['admin_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => t('Admin mode'),
    ];

    $existing_submission_terms = _uw_endowment_app_get_all_terms();
    $field_disabled = !$existing_submission_terms;
    if ($field_disabled) {
      $existing_submission_terms[] = 'No data';
    }

    $form['admin_wrapper']['admin_term'] = [
      '#type' => 'select',
      '#title' => t('Academic term'),
      '#options' => $existing_submission_terms,
      '#disabled' => $field_disabled,
      '#description' => t('For existing record you need to select Academic term in combination with user id.'),
    ];

    $form['admin_wrapper']['admin_input'] = [
      '#type' => 'textfield',
      '#title' => t('User ID or Receipt No'),
      '#description' => t('Provide Student user id or receipt (submission) number to load submission or contact API. If providing receipt number then academic term will be disregarded.'),
      '#ajax' => [
        'callback' => '_uw_endowment_app_info_ajax',
        'wrapper' => 'info-div',
        'method' => 'replace',
        'effect' => 'fade',
      ],
      '#size' => 10,
    ];
  }
  else {
    $help_message = variable_get('uw_endowment_app_message_help');

    if ($help_message && isset($help_message['value'])) {
      $form['help_message'] = [
        '#type' => 'markup',
        '#markup' => t($help_message['value']),
      ];
    }
  }

  $form['wrapper'] = [
    '#type' => 'fieldset',
    '#title' => t('Endowment request information'),
    '#prefix' => '<div id="info-div">',
    '#suffix' => '</div>',
  ];

  // Prepare data for currently loaded user, this includes admin
  $submission = _uw_endowment_app_student_data(FALSE, $admin_access);

  if (isset($form_state['values'])) {
    // If ajax is triggered, check admin fields and override previously loaded data with new data.
    $submission = _uw_endowment_app_process_admin_input($form_state['values']['admin_input'], $form_state['values']['admin_term'], $admin_access);
  }

  if (!$admin_access && !$submission['allow_update']) {
    return _uw_endowment_app_request_form_update_denied();
  }

  $form['wrapper']['sid'] = [
    '#type' => 'hidden',
    '#value' => isset($submission['sid']) ? $submission['sid'] : '',
  ];

  $form['wrapper']['status'] = [
    '#type' => 'hidden',
    '#value' => isset($submission['status']) ? $submission['status'] : '',
  ];

  $form['wrapper']['student_user_id'] = [
    '#type' => 'textfield',
    '#title' => t('Student User Id:'),
    '#default_value' => $submission['student_user_id'],
    '#disabled' => TRUE,
    '#size' => 10,
  ];

  $form['wrapper']['student_number'] = [
    '#type' => 'textfield',
    '#title' => t('Student Number:'),
    '#default_value' => $submission['student_number'],
    '#disabled' => TRUE,
    '#size' => 10,
  ];

  $form['wrapper']['student_email'] = [
    '#type' => 'textfield',
    '#title' => t('Student Email:'),
    '#default_value' => $submission['student_email'],
    '#disabled' => TRUE,
    '#size' => 40,
  ];

  $form['wrapper']['student_name'] = [
    '#type' => 'textfield',
    '#title' => t('Student Name:'),
    '#default_value' => $submission['student_name'],
    '#disabled' => TRUE,
    '#size' => 40,
  ];

  $form['wrapper']['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Endowment Fund Refund Request'),
  ];

  $intro = variable_get('uw_endowment_app_message_intro');

  if (!isset($intro['value'])) {
    $intro['value'] = '';
  }

  $form['wrapper']['group1']['instructions'] = [
    '#type' => 'markup',
    '#markup' => t($intro['value']),
  ];

  $refunds_default_value = [];
  if (isset($submission['refunds']) && !empty($submission['refunds'])) {
    $refunds_default_value = $submission['refunds'];
  }

  $available_refunds = list_extract_allowed_values(variable_get('uw_endowment_app_refund_types', []), 'list_text', FALSE);

  $form['wrapper']['group1']['refunds'] = [
    '#title' => t('Available refunds'),
    '#type' => 'checkboxes',
    '#options' => $available_refunds,
    '#required' => TRUE,
    '#default_value' => $refunds_default_value,
  ];

  $form['wrapper']['group1']['reasons'] = [
    '#type' => 'textarea',
    '#title' => t('Please tell us why you are requesting a refund'),
    '#rows' => 5,
    '#default_value' => $submission['reasons'],
  ];

  $form['wrapper']['group1']['opted_out'] = [
    '#type' => 'checkbox',
    '#title' => t('I have reviewed the benefits of contributing to the Endowment Fund &amp; choose to opt out.'),
    '#required' => TRUE,
    '#value' => $admin_access || FALSE,
  ];

  $form_status = isset($submission['status']) && is_numeric($submission['status']) ? (int)$submission['status'] : 0;

  $form['wrapper']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#validate' => ['uw_endowment_app_request_form_do_validate'],
    '#disabled' => $form_status === 2,
  ];

  if ($admin_access) {
    $form['wrapper']['submit']['#attributes']['class'] = ['button-green'];

    $form['wrapper']['cancel'] = [
      '#type' => 'submit',
      '#value' => $form_status === 1 ? t('Void') : t('Enable'),
      '#submit' => ['uw_endowment_app_request_form_void_submit'],
      '#attributes' => ['class' => ['button-orange']],
      '#disabled' => $form_status !== 1 && empty($submission['sid']),
    ];

    $form['wrapper']['remove'] = [
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => ['uw_endowment_app_request_form_delete_submit'],
      '#attributes' => ['class' => ['button-red']],
      '#disabled' => $form_status !== 2,
    ];
  }

  return $form;
}

/**
 * Custom validation function (not using default validate hook because of ajax).
 *
 * @param $form
 * @param $form_state
 */
function uw_endowment_app_request_form_do_validate($form, &$form_state) {
  // Check number of selected options, limit to 3
  $max_requests = variable_get('uw_endowment_app_max_requests', 3);
  $values = $form_state['values'];

  $requested_refunds = $values['refunds'];

  if ($requested_refunds
    && is_array($requested_refunds)
    && count(array_filter(array_values($requested_refunds))) > $max_requests
  ) {
    form_set_error('uw_refunds', t(variable_get('uw_endowment_app_message_error_max_req'), ['@maxrefunds' => $max_requests]));
  }

  if (!isset($values['student_user_id']) && !$values['student_user_id']) {
    form_set_error('student_user_id', t(variable_get('uw_endowment_app_message_error_missing_student_user_id')));
  }

  if (empty($values['student_number'])) {
    form_set_error('student_number', t(variable_get('uw_endowment_app_message_error_missing_student_number')));
  }
}

/**
 * Implements hook_submit().
 *
 * @param $form
 * @param $form_state
 */
function uw_endowment_app_request_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $values = $form_state['values'];

  // Sanitize data before saving to database, but remove array first
  $refunds_raw = array_keys(array_filter($values['refunds']));
  unset($values['refunds']);

  $refunds = [];
  $refunds_options = list_extract_allowed_values(variable_get('uw_endowment_app_refund_types', []), 'list_text', FALSE);

  foreach ($refunds_raw as $item) {
    $refunds[$item] = $refunds_options[$item];
  }

  $fields = [
    'student_email' => $values['student_email'],
    'student_name' => $values['student_name'],
    'reasons' => $values['reasons'],
    'status' => 1,
    'changed' => REQUEST_TIME,
    'refund' => implode(', ', array_keys($refunds)),
    'refunds' => $refunds,
    'term' => variable_get('uw_endowment_app_current_term'),
    'student_user_id' => $values['student_user_id'],
    'allow_update' => 0,
  ];

  $keys = [
    'student_number' => $values['student_number'],
    'term' => $fields['term'],
  ];


  try {
    module_load_include('inc', 'uw_endowment_app', 'includes/uw_endowment_app.submission');
    $receipt_number = _uw_endowment_app_submission_insert($fields, $keys);

    if (variable_get('uw_endowment_app_send_email_receipt') && $receipt_number) {
      module_load_include('inc', 'uw_endowment_app', 'includes/uw_endowment_app.email');

      $fields['receipt_no'] = $receipt_number['sid'];
      $fields['student_number'] = $values['student_number'];

      $email_send = uw_endowment_app_email_send_receipt_notification($fields['student_email'], $fields);

      if ($email_send) {
        drupal_set_message(t('Email receipt sent to @address', ['@address' => $fields['student_email']]));
      }
      else {
        drupal_set_message(t('There was an issue while sending email address!'), 'error');
      }
    }

    drupal_set_message(t('Request has been submitted/updated. Your receipt number is @num. Thank you.', ['@num' => $receipt_number['sid']]));
    cache_clear_all("uw_endowment_app:refunds:submissions", 'cache');
  } catch (Exception $ex) {
    drupal_set_message(t('There was an issue with saving your request'), 'error');
    watchdog('uw_endowment_app', $ex->getMessage(), NULL, WATCHDOG_CRITICAL);
  }
}

/**
 * Loads student api data based on provided information
 *
 * @param bool $student_user_id
 *
 * @return array|bool
 */
function _uw_endowment_app_get_student_api_data($student_user_id = FALSE) {
  if (!$student_user_id) {
    return FALSE;
  }

  $student_data = FALSE;

  module_load_include('inc', 'uw_php_apps_api', 'includes/uw_php_apps_api.functions');
  $api_user_data = uw_php_apps_api_user_data($student_user_id, 'uw_endowment_app', TRUE);

  if ($api_user_data) {
    $student_data = [
      'student_user_id' => $api_user_data['Username'],
      'student_number' => $api_user_data['StudentId'],
      'student_email' => $api_user_data['Email'],
      'student_name' => $api_user_data['Firstname'] . ' ' . $api_user_data['Lastname'],
    ];
  }

  return $student_data;
}


function _uw_endowment_app_info_ajax($form, $form_state) {
  return $form['wrapper'];
}

/**
 * If form is closed display message
 *
 * @return mixed
 *    Markup displayed when form is closed
 */
function _uw_endowment_app_request_form_closed() {
  $message = variable_get('uw_endowment_app_message_form_closed');
  $display = t('Form is closed for new requests and for submission updates.');

  if ($message && isset($message['value'])) {
    $display = $message['value'];
  }

  $form['info'] = [
    '#type' => 'markup',
    '#markup' => $display,
  ];

  return $form;
}

/**
 * If student lacks permission to update his/her previous permission.
 *
 * @return mixed
 *    Markup displayed when student has no permission to update data
 */
function _uw_endowment_app_request_form_update_denied() {
  $message = variable_get('uw_endowment_app_message_resubmission');
  $display = t("Sorry you don't have permission to update your previous submission");

  if ($message && isset($message['value'])) {
    $display = $message['value'];
  }

  $form['info'] = [
    '#type' => 'markup',
    '#markup' => $display,
  ];

  return $form;
}

/**
 * Load an existing submission, if that is not found, create array with logged in users data
 *
 * @param bool $student_user_id
 *  Optional. If not provided, currently logged in user cas username will be used if available
 * @param bool $admin_access
 *  Allow admin override
 *
 * @return array
 *  Always returns array with form fields as keys and default values
 */
function _uw_endowment_app_student_data($student_user_id = FALSE, $term = FALSE, $admin_access = FALSE) {
  $user_id = $student_user_id;
  $data = [
    'student_user_id' => '',
    'student_number' => '',
    'student_email' => '',
    'student_name' => '',
    'reasons' => FALSE,
    'refunds' => [],
    'allow_update' => 1,
  ];

  if (!$user_id) {
    $user_id = _uw_endowment_app_logged_in_user_data();
  }

  // Try to find an existing submission in case this is an update
  // Returning FALSE if no submission is found
  $submission = _uw_endowment_app_existing_submission($user_id, $term, $admin_access);

  if (!$submission) {
    // No submission is found, use API to get user data
    $user_data = _uw_endowment_app_get_student_api_data($user_id);
    $data = array_merge($data, $user_data);
  }
  else {
    $data = array_merge($data, $submission);
  }

  return $data;
}

function _uw_endowment_app_existing_submission($student_user_id = FALSE, $term = FALSE, $admin_mode = FALSE) {
  module_load_include('inc', 'uw_endowment_app', 'includes/uw_endowment_app.submission');
  $submission = _uw_endowment_app_submission_load($student_user_id, $term);

  if ($submission && isset($submission['sid'])) {
    if ($admin_mode || (isset($submission['status']) && $submission['status'])) {
      $submission['refunds'] = _uw_endowment_app_submission_refunds_load($submission['sid']);
    }
  }

  return $submission;
}

/**
 * Returns cas username id for currently logged in user
 *
 * @return bool | string
 *  FALSE - when user has no cas username associated
 *  cas username - when user has it assigned
 */
function _uw_endowment_app_logged_in_user_data() {
  global $user;

  $cas_username = FALSE;

  $users = [$user->uid => $user];
  cas_user_load($users);

  if ($user->uid && count($users) && isset($users[$user->uid]) && isset($users[$user->uid]->cas_name)) {
    $cas_username = $users[$user->uid]->cas_name;
  }

  return $cas_username;
}

function uw_endowment_app_request_form_void_submit($form, &$form_state) {
  $values = $form_state['values'];

  if (isset($values['sid']) && is_numeric($values['sid']) && is_numeric($values['status'])) {
    $status = (int) $values['status'];

    module_load_include('inc', 'uw_endowment_app', 'includes/uw_endowment_app.submission');

    $new_status = 1;

    if ($status === 1) {
      $new_status = 2;
    }
    elseif ($status === 2) {
      $new_status = 1;
    }

    $result = _uw_endowment_app_submission_state_update($values['sid'], $new_status);

    if ($result) {
      drupal_set_message(t('Updated submission with receipt number: @num. Changed status to @newstatus.', ['@num' => $values['sid'], '@newstatus' => $new_status]));
    }
    else {
      drupal_set_message(t('There was an issue while updating status of submission with receipt number: @num.', ['@num' => $values['sid']]), 'error');
    }
  }
}

function uw_endowment_app_request_form_delete_submit($form, &$form_state) {
  $values = $form_state['values'];

  if (isset($values['sid']) && is_numeric($values['sid'])) {
    module_load_include('inc', 'uw_endowment_app', 'includes/uw_endowment_app.submission');
    $result = _uw_endowment_app_submission_delete($values['sid']);

    if ($result) {
      drupal_set_message(t('Deleted submission with receipt number: @num.', ['@num' => $values['sid']]));
    }
    else {
      drupal_set_message(t('There was an issue with deleting submission number with receipt: @num.', ['@num' => $values['sid']]));
    }
  }
}

function uw_endowment_app_update_submission_delete_prepare($form, &$form_state, $receipt_no) {
  if ($receipt_no && is_numeric($receipt_no)) {
    $admin_mode = variable_get('uw_endowment_app_admin_mode');

    if ($admin_mode && user_access('administer uwaterloo endowment app')) {
      $form = [];
      $form['sid'] = [
        '#type' => 'value',
        '#value' => $receipt_no,
      ];

      return confirm_form(
        $form,
        t('Are you sure you want to delete submission?'),
        'admin/endowment/data',
        t('This action cannot be undone. Submission with receipt number @num will be deleted.', ['@num' => $receipt_no]),
        t('Delete'),
        t('Cancel')
      );
    }
  }

  return FALSE;
}

/**
 * Submit handler for delete logic
 *
 * @param $form
 * @param $form_state
 */
function uw_endowment_app_update_submission_delete_prepare_submit($form, &$form_state) {
  $values = $form_state['values'];

  if (isset($values['sid'])) {
    $sid = $values['sid'];

    if (is_numeric($sid)) {
      module_load_include('inc', 'uw_endowment_app', 'includes/uw_endowment_app.submissions');

      try {
        $result = _uw_endowment_app_submission_delete($sid);

        if ($result) {
          drupal_set_message(t('Submission with receipt number @num has been deleted.', ['@num' => $sid]));
        }
      }catch (Exception $ex) {
        drupal_set_message(t('There was an error deleting endowment submissions having receipt number @num.', ['@num' => $sid]), 'error');
        watchdog('uw_endowment_app', 'Exception on receipt (@num) delete: @message', ['@num' => $sid, '@message' => $ex->getMessage()], WATCHDOG_ERROR);
      }
    }
  }

  drupal_goto('admin/endowment/data');
}

function _uw_endowment_app_process_admin_input($admin_input = NULL, $admin_term, $admin_access = FALSE) {
  $result = FALSE;

  if ($admin_input) {
    module_load_include('inc', 'uw_endowment_app', 'includes/uw_endowment_app.submission');

    if (is_numeric($admin_input)) {
      // No need for academic term since we are loading per receipt.
      $result = _uw_endowment_app_submission_load_receipt($admin_input);
    }
    else {
      // Passing academic term and loading submission based on term as well.
      $result = _uw_endowment_app_student_data($admin_input, $admin_term, $admin_access);
    }

    if ($result && isset($result['sid'])) {
      if ($admin_access || (isset($result['status']) && $result['status'])) {
        $result['refunds'] = _uw_endowment_app_submission_refunds_load($result['sid']);
      }
    }
  }

  return $result;
}

function _uw_endowment_app_get_all_terms() {
  $current_term = variable_get('uw_endowment_app_current_term');

  module_load_include('inc', 'uw_endowment_app', 'includes/uw_endowment_app.submission');

  $all_terms = _uw_endowment_app_academic_terms();

  if ($all_terms) {
    $all_terms = drupal_map_assoc($all_terms);

    if (isset($all_terms[$current_term])) {
      $all_terms[$current_term] .= ' - Current';
    }
  }

  return $all_terms;
}