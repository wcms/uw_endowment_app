<?php

/**
 * @file
 *
 * Submissions CRUD actions.
 */


/**
 * Creates or updates submission record based on keys provided.
 *
 * @param array $fields
 *    Associative array with fields to insert or update
 * @param array $keys
 *    Associative array of keys, which can be student_number, uw_user_id, or even sid
 *
 * @return int
 *    Order/submission id
 * @throws \Exception
 *    If keys are missing throw exception to prevent any insert/update
 */
function _uw_endowment_app_submission_insert(array $fields, array $keys) {
  if (!isset($keys['student_number']) || !isset($keys['term'])) {
    throw new Exception(t('Insert submission requires student_number and term to be provided in keys array'));
  }

  $refunds = isset($fields['refunds']) ? $fields['refunds'] : FALSE;

  if (!$refunds) {
    throw new Exception(t('Missing refunds for submission'));
  }

  // Remove refunds since those are processed differently
  unset($fields['refunds']);
  $fields['refund'] = implode(', ', array_keys($refunds));

  db_merge(UW_ENDOWMENT_APP_SUBMISSIONS_TABLE)
    ->insertFields($fields +
      [
        'created' => REQUEST_TIME,
      ])
    ->updateFields($fields)
    ->key([
      'student_number' => $keys['student_number'],
      'term' => $keys['term'],
    ])
    ->execute();

  // Getting submission record for submission id/receipt number.
  $receipt_number = db_select(UW_ENDOWMENT_APP_SUBMISSIONS_TABLE, 'st')
    ->fields('st', ['sid'])
    ->condition('student_number', $keys['student_number'])
    ->condition('term', $keys['term'])
    ->execute()
    ->fetchAssoc();

  // Changing status of previous entries, them db_merge will happen that will set status to 1. Last step would be
  // to delete all entries with status <> 1 for that student number.
  $num_updated = db_update(UW_ENDOWMENT_APP_SUBMISSIONS_REFUND_TABLE)
    ->fields([
      'status' => 0,
      'created' => REQUEST_TIME,
    ])
    ->condition('rsid', $receipt_number)
    ->execute();

  foreach ($refunds as $code => $value) {
    $record = [
      'code' => $code,
      'value' => $value,
      'status' => 1,
      'created' => REQUEST_TIME,
    ];

    db_merge(UW_ENDOWMENT_APP_SUBMISSIONS_REFUND_TABLE)
      ->fields($record)
      ->key(['rsid' => $receipt_number, 'code' => $code])
      ->execute();
  }

  // Check if there were previous submissions, if so, delete ones with status 0, updated records should have status 1
  if ($num_updated) {
    db_delete(UW_ENDOWMENT_APP_SUBMISSIONS_REFUND_TABLE)
      ->condition('rsid', $receipt_number)
      ->condition('status', 0)
      ->execute();
  }

  return $receipt_number;
}

function _uw_endowment_app_submission_allow_update_toggle($sid) {
  $updated = 0;
  if ($sid && is_numeric($sid)) {
    $id = intval($sid);

    if ($id == $sid) {
      $updated = db_update('uw_endowment_app_submissions')
        ->expression('allow_update', 'NOT allow_update')
        ->condition('sid', $id)
        ->execute();
    }
  }

  return $updated;
}

function _uw_endowment_app_submission_refund_distinct($keys) {
  $key = (count($keys) > 0) ? $keys[0] : '';

  $result = db_select(UW_ENDOWMENT_APP_SUBMISSIONS_REFUND_TABLE, 'st')
    ->fields('st', $keys)
    ->distinct()
    ->execute()
    ->fetchAllAssoc($key);

  return $result;
}

function _uw_endowment_app_submission_term_distinct() {
  $result = db_select(UW_ENDOWMENT_APP_SUBMISSIONS_TABLE, 'st')
    ->fields('st', ['term'])
    ->distinct()
    ->execute()
    ->fetchAllAssoc('term');

  return $result;
}

function _uw_endowment_app_submission_load($user_id, $term = FALSE) {
  $submission = FALSE;
  $submission_term = $term;

  if (!$submission_term) {
    $submission_term = variable_get('uw_endowment_app_current_term');
  }

  if ($submission_term) {
    $submission = db_select(UW_ENDOWMENT_APP_SUBMISSIONS_TABLE, 'uest')
      ->fields('uest')
      ->condition('student_user_id', $user_id)
      ->condition('term', $submission_term)
      ->execute()
      ->fetchAssoc();
  }

  return $submission;
}

function _uw_endowment_app_submission_refunds_load($sid) {
  $refunds = db_select(UW_ENDOWMENT_APP_SUBMISSIONS_REFUND_TABLE, 'rt')
    ->fields('rt', ['code'])
    ->condition('rsid', $sid)
    ->execute()
    ->fetchAllAssoc('code');

  return array_keys($refunds);
}

/**
 * Updating status in both tables.
 *
 * @param $receipt_no
 * @param int $status
 *
 * @return bool|\DatabaseStatementInterface
 */
function _uw_endowment_app_submission_state_update($receipt_no, $status = 1) {
  $result = FALSE;

  if ($receipt_no && is_numeric($receipt_no) && is_numeric($status)) {
    $fields['status'] = $status;
    $fields['changed'] = REQUEST_TIME;

    $result = db_update(UW_ENDOWMENT_APP_SUBMISSIONS_TABLE)
      ->fields($fields)
      ->condition('sid', $receipt_no)
      ->execute();

    if ($result) {
      $refund_status = 1;

      if ($status === 2) {
        $refund_status = 0;
      }

      $result = db_update(UW_ENDOWMENT_APP_SUBMISSIONS_REFUND_TABLE)
        ->fields(['status' => $refund_status])
        ->condition('rsid', $receipt_no)
        ->execute();
    }
  }

  return $result;
}

/**
 * Deleting submissions from both tables
 *
 * @param $receipt_no
 *
 * @return bool|\DatabaseStatementInterface
 */
function _uw_endowment_app_submission_delete($receipt_no) {
  $result = FALSE;

  // In order to actually delete, receipt needs to be numeric and status needs to be void (2).
  if ($receipt_no && is_numeric($receipt_no)) {
    $result = db_delete(UW_ENDOWMENT_APP_SUBMISSIONS_TABLE)
      ->condition('sid', $receipt_no)
      ->condition('status', 2)
      ->execute();

    if ($result) {
      $result = db_delete(UW_ENDOWMENT_APP_SUBMISSIONS_REFUND_TABLE)
        ->condition('rsid', $receipt_no)
        ->execute();
    }
  }

  return $result;
}


/**
 * Load submission based on receipt number.
 *
 * @param $receipt_no
 * @param bool $term
 *
 * @return bool
 */
function _uw_endowment_app_submission_load_receipt($receipt_no) {
  $submission = FALSE;

  if ($receipt_no && is_numeric($receipt_no)) {
    $submission = db_select(UW_ENDOWMENT_APP_SUBMISSIONS_TABLE, 'uest')
      ->fields('uest')
      ->condition('sid', $receipt_no)
      ->execute()
      ->fetchAssoc();
  }

  return $submission;
}

/**
 * Get DISTINCT values for submission's academic terms. Used only in admin mode.
 *
 * @return array|bool
 */
function _uw_endowment_app_academic_terms() {
  $result = db_select(UW_ENDOWMENT_APP_SUBMISSIONS_TABLE, 'uwaps')
    ->distinct()
    ->fields('uwaps', ['term'])
    ->orderBy('term', 'DESC')
    ->execute()
    ->fetchAllAssoc('term');

  return $result ? array_keys($result) : FALSE;
}
