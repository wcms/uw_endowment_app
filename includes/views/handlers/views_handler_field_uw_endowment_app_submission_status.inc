<?php

/**
 * @file
 *
 * Custom views handler that parses refunds from separate table and displays
 * data nicely on submission view/table.
 */

class views_handler_field_uw_endowment_app_submission_status extends views_handler_field {

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['format'] = [
      '#type' => 'select',
      '#title' => t('Display format'),
      '#description' => t('How should the refund data be displayed. You can choose a custom array/object key or a print_r on the full output.'),
      '#options' => [
        'label_only' => t('Label only'),
        'label_code' => t('Label and code'),
        'code_only' => t('Code only'),
      ],
      '#default_value' => $this->options['format'],
    ];
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['format'] = ['default' => 'label_code'];

    return $options;
  }

  function render($values) {
    $value = $this->get_value($values);
    $result = '';

    if (!$value) {
      return $result;
    }

    $mapping = [
      0 => t('New'),
      1 => t('Submitted'),
      2 => t('Void'),
    ];

    $format = $this->options['format'];

    if (isset($mapping[$value])) {

      if ($format === 'label_only') {
        $result .= $mapping[$value];
      }
      elseif ($format === 'label_code') {
        $result .= "$mapping[$value] ($value)";
      }
      elseif ($format === 'code_only') {
        $result .= $value;
      }
    }

    return $result;
  }
}
