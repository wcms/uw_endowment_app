<?php

/**
 * @file
 *
 * Views exposure of UW Endowment
 */

/**
 * Implements hook_views_data().
 */
function uw_endowment_app_views_data() {
  $data = [];

  if (!class_exists('views_handler_field_uw_endowment_app_submission_status')) {
    module_load_include('inc', 'uw_endowment_app', 'views/handlers/views_handler_field_uw_endowment_app_submission_status');
  }

  $submission_table = UW_ENDOWMENT_APP_SUBMISSIONS_TABLE;

  $data[$submission_table]['table']['group'] = t('UW Endowment Submissions');
  $data[$submission_table]['table']['base'] = [
    'title' => t('UW Endowment Submission'),
    'help' => t('Students Endowment submissions.'),
  ];

  $data[$submission_table]['sid'] = [
    'title' => t('Submission ID'),
    'help' => t('Unique number for submission. Can be used as receipt id.'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
  ];

  $data[$submission_table]['student_user_id'] = [
    'title' => t('UW User ID'),
    'help' => t('UW User identifier.'),
    'field' => [
      'handler' => 'views_handler_field',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
  ];

  $data[$submission_table]['student_number'] = [
    'title' => t('Student number'),
    'help' => t('UW Student number.'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
  ];

  $data[$submission_table]['student_email'] = [
    'title' => t('Email'),
    'help' => t('Student email used on request.'),
    'field' => [
      'handler' => 'views_handler_field',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
  ];

  $data[$submission_table]['student_name'] = [
    'title' => t('Full Name'),
    'help' => t('Student first and last name as used on request.'),
    'field' => [
      'handler' => 'views_handler_field',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
  ];

  $data[$submission_table]['reasons'] = [
    'title' => t('Reasons'),
    'help' => t('Student reasons from request.'),
    'field' => [
      'handler' => 'views_handler_field',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
  ];

  $data[$submission_table]['allow_update'] = [
    'title' => t('Allow update'),
    'help' => t('Are updates allowed or not.'),
    'field' => [
      'handler' => 'views_handler_field_boolean',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_boolean_operator',
    ],
  ];

  $data[$submission_table]['status'] = [
    'title' => t('Status'),
    'help' => t('Submission status, submitted or void'),
    'field' => [
      'handler' => 'views_handler_field_uw_endowment_app_submission_status',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
  ];

  $data[$submission_table]['created'] = [
    'title' => t('Submission date'), // The item it appears as on the UI,
    'help' => t('The date the submission was created.'), // The help that appears on the UI,
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
  ];

  $data[$submission_table]['changed'] = [
    'title' => t('Submission update'), // The item it appears as on the UI,
    'help' => t('The date the submission was updated.'), // The help that appears on the UI,
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
  ];

  $data[$submission_table]['refund'] = [
    'title' => t('Refunds'),
    'help' => t('Student requested refunds.'),
    'field' => [
      'handler' => 'views_handler_field',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
  ];

  $data[$submission_table]['term'] = [
    'title' => t('Academic Term'),
    'help' => t('Term as save on configuration page.'),
    'field' => [
      'handler' => 'views_handler_field',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
  ];

  $refund_table = UW_ENDOWMENT_APP_SUBMISSIONS_REFUND_TABLE;

  $data[$refund_table]['table']['group'] = t('UW Endowment Submissions');
  $data[$refund_table]['table']['base'] = [
    'title' => t('UW Endowment Refunds'),
    'help' => t('Students Endowment submissions with refunds.'),
  ];

  $data[$refund_table]['rsid'] = [
    'title' => t('Submission Id'),
    'help' => t('Student that submitted request. If you need more fields make sure to add relationship to submission table.'),
    'relationship' => [
      'title' => t('Submission data'),
      'help' => t('Relate content to the submission record.'),
      'handler' => 'views_handler_relationship',
      'base' => UW_ENDOWMENT_APP_SUBMISSIONS_TABLE,
      'label' => t('submission'),
      'base field' => 'sid',
      'field' => 'rsid',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
    'argument' => [
      'handler' => 'views_handler_argument_numeric',
    ],
    'field' => [
      'handler' => 'views_handler_field',
    ],
  ];

  $data[$refund_table]['code'] = [
    'title' => t('Refund code'),
    'help' => t('Refund identifier.'),
    'field' => [
      'handler' => 'views_handler_field',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
  ];

  $data[$refund_table]['value'] = [
    'title' => t('Refund description'),
    'help' => t('Description for specific refund, includes price.'),
    'field' => [
      'handler' => 'views_handler_field',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
  ];

  $data[$refund_table]['status'] = [
    'title' => t('Status'),
    'help' => t('Status for specific refund in export table view.'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
  ];

  return $data;
}

