<?php

/**
 * @file
 *
 * Default views from UW Endowment module
 */

/**
 * Implements hook_views_default_views().
 */
function uw_endowment_app_views_default_views() {
  $view_submissions = _uw_endowment_app_view_submissions();
  $view_submissions_export = _uw_endowment_app_view_submissions_export();

  $views = [
    $view_submissions_export->name => $view_submissions_export,
    $view_submissions->name => $view_submissions,
  ];

  return $views;
}

/**
 * Exported view - Submissions
 */
function _uw_endowment_app_view_submissions() {
  $view = new view();
  $view->name = 'uw_endowment_app_submissions';
  $view->description = 'Student submitted Endowment requests';
  $view->tag = 'default';
  $view->base_table = 'uw_endowment_app_submissions';
  $view->human_name = 'UW Endowment Submissions';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'UW Endowment Submissions';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer uwaterloo endowment app';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'sid' => 'sid',
    'student_email' => 'student_email',
    'student_name' => 'student_name',
    'reasons' => 'reasons',
    'refunds' => 'refunds',
    'status' => 'status',
    'student_number' => 'student_number',
    'uw_user_id' => 'uw_user_id',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'sid' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'student_email' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'student_name' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'reasons' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'refunds' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'status' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'student_number' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'uw_user_id' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No data to display.';
  $handler->display->display_options['empty']['area']['format'] = 'uw_tf_standard';
  /* Field: UW Endowment Submissions: Submission ID */
  $handler->display->display_options['fields']['sid']['id'] = 'sid';
  $handler->display->display_options['fields']['sid']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['sid']['field'] = 'sid';
  $handler->display->display_options['fields']['sid']['label'] = 'Receipt No';
  $handler->display->display_options['fields']['sid']['separator'] = '';
  /* Field: UW Endowment Submissions: UW User ID */
  $handler->display->display_options['fields']['student_user_id']['id'] = 'student_user_id';
  $handler->display->display_options['fields']['student_user_id']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['student_user_id']['field'] = 'student_user_id';
  $handler->display->display_options['fields']['student_user_id']['label'] = 'User ID';
  /* Field: UW Endowment Submissions: Student number */
  $handler->display->display_options['fields']['student_number']['id'] = 'student_number';
  $handler->display->display_options['fields']['student_number']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['student_number']['field'] = 'student_number';
  $handler->display->display_options['fields']['student_number']['separator'] = '';
  /* Field: UW Endowment Submissions: Full Name */
  $handler->display->display_options['fields']['student_name']['id'] = 'student_name';
  $handler->display->display_options['fields']['student_name']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['student_name']['field'] = 'student_name';
  /* Field: UW Endowment Submissions: Email */
  $handler->display->display_options['fields']['student_email']['id'] = 'student_email';
  $handler->display->display_options['fields']['student_email']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['student_email']['field'] = 'student_email';
  /* Field: UW Endowment Submissions: Academic Term */
  $handler->display->display_options['fields']['term']['id'] = 'term';
  $handler->display->display_options['fields']['term']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['term']['field'] = 'term';
  $handler->display->display_options['fields']['term']['label'] = 'Term';
  /* Field: UW Endowment Submissions: Allow update */
  $handler->display->display_options['fields']['allow_update']['id'] = 'allow_update';
  $handler->display->display_options['fields']['allow_update']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['allow_update']['field'] = 'allow_update';
  $handler->display->display_options['fields']['allow_update']['label'] = 'Update';
  $handler->display->display_options['fields']['allow_update']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['allow_update']['alter']['path'] = 'admin/endowment/[sid]/allow_update';
  $handler->display->display_options['fields']['allow_update']['type'] = 'enabled-disabled';
  $handler->display->display_options['fields']['allow_update']['not'] = 0;
  /* Field: UW Endowment Submissions: Submission date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'short';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
  /* Field: UW Endowment Submissions: Submission update */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['date_format'] = 'short';
  $handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
  /* Field: UW Endowment Submissions: Refunds */
  $handler->display->display_options['fields']['refund']['id'] = 'refund';
  $handler->display->display_options['fields']['refund']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['refund']['field'] = 'refund';
  /* Field: UW Endowment Submissions: Reasons */
  $handler->display->display_options['fields']['reasons']['id'] = 'reasons';
  $handler->display->display_options['fields']['reasons']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['reasons']['field'] = 'reasons';
  /* Field: UW Endowment Submissions: Status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['format'] = 'label_only';
  /* Sort criterion: UW Endowment Submissions: Submission ID */
  $handler->display->display_options['sorts']['sid']['id'] = 'sid';
  $handler->display->display_options['sorts']['sid']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['sorts']['sid']['field'] = 'sid';
  $handler->display->display_options['sorts']['sid']['order'] = 'DESC';
  /* Filter criterion: UW Endowment Submissions: Submission ID */
  $handler->display->display_options['filters']['sid']['id'] = 'sid';
  $handler->display->display_options['filters']['sid']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['filters']['sid']['field'] = 'sid';
  $handler->display->display_options['filters']['sid']['group'] = 1;
  $handler->display->display_options['filters']['sid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['sid']['expose']['operator_id'] = 'sid_op';
  $handler->display->display_options['filters']['sid']['expose']['label'] = 'Receipt No';
  $handler->display->display_options['filters']['sid']['expose']['operator'] = 'sid_op';
  $handler->display->display_options['filters']['sid']['expose']['identifier'] = 'sid';
  $handler->display->display_options['filters']['sid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    21 => 0,
    16 => 0,
    19 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    18 => 0,
    20 => 0,
    17 => 0,
  );
  /* Filter criterion: UW Endowment Submissions: Student number */
  $handler->display->display_options['filters']['student_number']['id'] = 'student_number';
  $handler->display->display_options['filters']['student_number']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['filters']['student_number']['field'] = 'student_number';
  $handler->display->display_options['filters']['student_number']['group'] = 1;
  $handler->display->display_options['filters']['student_number']['exposed'] = TRUE;
  $handler->display->display_options['filters']['student_number']['expose']['operator_id'] = 'student_number_op';
  $handler->display->display_options['filters']['student_number']['expose']['label'] = 'Student number';
  $handler->display->display_options['filters']['student_number']['expose']['operator'] = 'student_number_op';
  $handler->display->display_options['filters']['student_number']['expose']['identifier'] = 'student_number';
  $handler->display->display_options['filters']['student_number']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    21 => 0,
    16 => 0,
    19 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    18 => 0,
    20 => 0,
    17 => 0,
  );
  /* Filter criterion: UW Endowment Submissions: Full Name */
  $handler->display->display_options['filters']['student_name']['id'] = 'student_name';
  $handler->display->display_options['filters']['student_name']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['filters']['student_name']['field'] = 'student_name';
  $handler->display->display_options['filters']['student_name']['operator'] = 'contains';
  $handler->display->display_options['filters']['student_name']['group'] = 1;
  $handler->display->display_options['filters']['student_name']['exposed'] = TRUE;
  $handler->display->display_options['filters']['student_name']['expose']['operator_id'] = 'student_name_op';
  $handler->display->display_options['filters']['student_name']['expose']['label'] = 'Full Name';
  $handler->display->display_options['filters']['student_name']['expose']['operator'] = 'student_name_op';
  $handler->display->display_options['filters']['student_name']['expose']['identifier'] = 'student_name';
  $handler->display->display_options['filters']['student_name']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    21 => 0,
    16 => 0,
    19 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    18 => 0,
    20 => 0,
    17 => 0,
  );
  /* Filter criterion: UW Endowment Submissions: Email */
  $handler->display->display_options['filters']['student_email']['id'] = 'student_email';
  $handler->display->display_options['filters']['student_email']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['filters']['student_email']['field'] = 'student_email';
  $handler->display->display_options['filters']['student_email']['operator'] = 'contains';
  $handler->display->display_options['filters']['student_email']['group'] = 1;
  $handler->display->display_options['filters']['student_email']['exposed'] = TRUE;
  $handler->display->display_options['filters']['student_email']['expose']['operator_id'] = 'student_email_op';
  $handler->display->display_options['filters']['student_email']['expose']['label'] = 'Email';
  $handler->display->display_options['filters']['student_email']['expose']['operator'] = 'student_email_op';
  $handler->display->display_options['filters']['student_email']['expose']['identifier'] = 'student_email';
  $handler->display->display_options['filters']['student_email']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    21 => 0,
    16 => 0,
    19 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    18 => 0,
    20 => 0,
    17 => 0,
  );
  /* Filter criterion: UW Endowment Submissions: Refunds */
  $handler->display->display_options['filters']['refund']['id'] = 'refund';
  $handler->display->display_options['filters']['refund']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['filters']['refund']['field'] = 'refund';
  $handler->display->display_options['filters']['refund']['exposed'] = TRUE;
  $handler->display->display_options['filters']['refund']['expose']['operator_id'] = 'refund_op';
  $handler->display->display_options['filters']['refund']['expose']['label'] = 'Refunds';
  $handler->display->display_options['filters']['refund']['expose']['operator'] = 'refund_op';
  $handler->display->display_options['filters']['refund']['expose']['identifier'] = 'refund';
  $handler->display->display_options['filters']['refund']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['refund']['group_info']['label'] = 'Refunds';
  $handler->display->display_options['filters']['refund']['group_info']['identifier'] = 'refund';
  $handler->display->display_options['filters']['refund']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'AHS',
      'operator' => 'regular_expression',
      'value' => 'AHS',
    ),
    2 => array(
      'title' => 'ACCTG',
      'operator' => 'regular_expression',
      'value' => 'ACCTG',
    ),
    3 => array(
      'title' => 'ARTS',
      'operator' => 'regular_expression',
      'value' => 'ARTS',
    ),
    4 => array(
      'title' => 'WEEF',
      'operator' => 'regular_expression',
      'value' => 'WEEF',
    ),
    5 => array(
      'title' => 'WEEF_SE',
      'operator' => 'regular_expression',
      'value' => 'WEEF_SE',
    ),
    6 => array(
      'title' => 'ENV',
      'operator' => 'regular_expression',
      'value' => 'ENV',
    ),
    7 => array(
      'title' => 'GSEF',
      'operator' => 'regular_expression',
      'value' => 'GSEF',
    ),
    8 => array(
      'title' => 'MATH',
      'operator' => 'regular_expression',
      'value' => 'MATH',
    ),
    9 => array(
      'title' => 'MATH_SE',
      'operator' => 'regular_expression',
      'value' => 'MATH_SE',
    ),
    10 => array(
      'title' => 'SCI',
      'operator' => 'regular_expression',
      'value' => 'SCI',
    ),
    11 => array(
      'title' => 'SFD',
      'operator' => 'regular_expression',
      'value' => 'SFD',
    ),
  );
  /* Filter criterion: UW Endowment Submissions: Academic Term */
  $handler->display->display_options['filters']['term']['id'] = 'term';
  $handler->display->display_options['filters']['term']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['filters']['term']['field'] = 'term';
  $handler->display->display_options['filters']['term']['exposed'] = TRUE;
  $handler->display->display_options['filters']['term']['expose']['operator_id'] = 'term_op';
  $handler->display->display_options['filters']['term']['expose']['label'] = 'Academic Term';
  $handler->display->display_options['filters']['term']['expose']['operator'] = 'term_op';
  $handler->display->display_options['filters']['term']['expose']['identifier'] = 'term';
  $handler->display->display_options['filters']['term']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    21 => 0,
    16 => 0,
    19 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    18 => 0,
    20 => 0,
    17 => 0,
    22 => 0,
  );
  $handler->display->display_options['filters']['term']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['term']['group_info']['label'] = 'Academic Term';
  $handler->display->display_options['filters']['term']['group_info']['identifier'] = 'term';
  $handler->display->display_options['filters']['term']['group_info']['group_items'] = array(
    1 => array(
      'title' => '',
      'operator' => '=',
      'value' => '',
    ),
  );
  /* Filter criterion: UW Endowment Submissions: Allow update */
  $handler->display->display_options['filters']['allow_update']['id'] = 'allow_update';
  $handler->display->display_options['filters']['allow_update']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['filters']['allow_update']['field'] = 'allow_update';
  $handler->display->display_options['filters']['allow_update']['value'] = 'All';
  $handler->display->display_options['filters']['allow_update']['exposed'] = TRUE;
  $handler->display->display_options['filters']['allow_update']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['allow_update']['expose']['label'] = 'Allow update';
  $handler->display->display_options['filters']['allow_update']['expose']['operator'] = 'allow_update_op';
  $handler->display->display_options['filters']['allow_update']['expose']['identifier'] = 'allow_update';
  $handler->display->display_options['filters']['allow_update']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    21 => 0,
    16 => 0,
    19 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    18 => 0,
    20 => 0,
    17 => 0,
    28 => 0,
    33 => 0,
  );
  /* Filter criterion: UW Endowment Submissions: Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['exposed'] = TRUE;
  $handler->display->display_options['filters']['status']['expose']['operator_id'] = 'status_op';
  $handler->display->display_options['filters']['status']['expose']['label'] = 'Status';
  $handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
  $handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
  $handler->display->display_options['filters']['status']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['status']['group_info']['label'] = 'Status';
  $handler->display->display_options['filters']['status']['group_info']['identifier'] = 'status';
  $handler->display->display_options['filters']['status']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'Submitted',
      'operator' => '=',
      'value' => array(
        'value' => '1',
        'min' => '',
        'max' => '',
      ),
    ),
    2 => array(
      'title' => 'Void',
      'operator' => '=',
      'value' => array(
        'value' => '2',
        'min' => '',
        'max' => '',
      ),
    ),
    3 => array(
      'title' => '',
      'operator' => '=',
      'value' => array(
        'value' => '',
        'min' => '',
        'max' => '',
      ),
    ),
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['header'] = FALSE;
  $handler->display->display_options['path'] = 'admin/endowment/data';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Data';
  $handler->display->display_options['menu']['weight'] = '50';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $handler->display->display_options['tab_options']['title'] = 'Data';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $translatables['uw_endowment_app_submissions'] = array(
    t('Master'),
    t('UW Endowment Submissions'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('No data to display.'),
    t('Receipt No'),
    t('.'),
    t('User ID'),
    t('Student number'),
    t('Full Name'),
    t('Email'),
    t('Term'),
    t('Update'),
    t('Submission date'),
    t('Submission update'),
    t('Refunds'),
    t('Reasons'),
    t('Status'),
    t('Academic Term'),
    t('Allow update'),
    t('Page'),
  );

  return $view;
}

/**
 * Exported view - Exports
 */
function _uw_endowment_app_view_submissions_export() {
  $view = new view();
  $view->name = 'uw_endowment_app_submissions_export';
  $view->description = 'Export of Endowment Requests one refund per one student per one line.';
  $view->tag = 'default';
  $view->base_table = 'uw_endowment_app_submissions_refunds';
  $view->human_name = 'UW Endowment Submissions Export';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Refunds Export';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer uwaterloo endowment app';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['total_pages'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Relationship: UW Endowment Submissions: Submission data */
  $handler->display->display_options['relationships']['rsid']['id'] = 'rsid';
  $handler->display->display_options['relationships']['rsid']['table'] = 'uw_endowment_app_submissions_refunds';
  $handler->display->display_options['relationships']['rsid']['field'] = 'rsid';
  $handler->display->display_options['relationships']['rsid']['required'] = TRUE;
  /* Field: UW Endowment Submissions: Student number */
  $handler->display->display_options['fields']['student_number']['id'] = 'student_number';
  $handler->display->display_options['fields']['student_number']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['student_number']['field'] = 'student_number';
  $handler->display->display_options['fields']['student_number']['relationship'] = 'rsid';
  $handler->display->display_options['fields']['student_number']['separator'] = '';
  /* Field: UW Endowment Submissions: Full Name */
  $handler->display->display_options['fields']['student_name']['id'] = 'student_name';
  $handler->display->display_options['fields']['student_name']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['student_name']['field'] = 'student_name';
  $handler->display->display_options['fields']['student_name']['relationship'] = 'rsid';
  $handler->display->display_options['fields']['student_name']['label'] = 'Name';
  /* Field: UW Endowment Submissions: Submission ID */
  $handler->display->display_options['fields']['sid']['id'] = 'sid';
  $handler->display->display_options['fields']['sid']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['sid']['field'] = 'sid';
  $handler->display->display_options['fields']['sid']['relationship'] = 'rsid';
  $handler->display->display_options['fields']['sid']['label'] = 'Receipt No';
  $handler->display->display_options['fields']['sid']['separator'] = '';
  /* Field: UW Endowment Submissions: Submission update */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['relationship'] = 'rsid';
  $handler->display->display_options['fields']['changed']['label'] = 'Date';
  $handler->display->display_options['fields']['changed']['date_format'] = 'short';
  $handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
  /* Field: UW Endowment Submissions: UW User ID */
  $handler->display->display_options['fields']['student_user_id']['id'] = 'student_user_id';
  $handler->display->display_options['fields']['student_user_id']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['student_user_id']['field'] = 'student_user_id';
  $handler->display->display_options['fields']['student_user_id']['relationship'] = 'rsid';
  $handler->display->display_options['fields']['student_user_id']['label'] = 'User ID';
  /* Field: UW Endowment Submissions: Email */
  $handler->display->display_options['fields']['student_email']['id'] = 'student_email';
  $handler->display->display_options['fields']['student_email']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['student_email']['field'] = 'student_email';
  $handler->display->display_options['fields']['student_email']['relationship'] = 'rsid';
  /* Field: UW Endowment Submissions: Academic Term */
  $handler->display->display_options['fields']['term']['id'] = 'term';
  $handler->display->display_options['fields']['term']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['term']['field'] = 'term';
  $handler->display->display_options['fields']['term']['relationship'] = 'rsid';
  /* Field: UW Endowment Submissions: Refund code */
  $handler->display->display_options['fields']['code']['id'] = 'code';
  $handler->display->display_options['fields']['code']['table'] = 'uw_endowment_app_submissions_refunds';
  $handler->display->display_options['fields']['code']['field'] = 'code';
  /* Field: UW Endowment Submissions: Reasons */
  $handler->display->display_options['fields']['reasons']['id'] = 'reasons';
  $handler->display->display_options['fields']['reasons']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['fields']['reasons']['field'] = 'reasons';
  /* Sort criterion: UW Endowment Submissions: Submission ID */
  $handler->display->display_options['sorts']['sid']['id'] = 'sid';
  $handler->display->display_options['sorts']['sid']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['sorts']['sid']['field'] = 'sid';
  $handler->display->display_options['sorts']['sid']['relationship'] = 'rsid';
  $handler->display->display_options['sorts']['sid']['order'] = 'DESC';
  $handler->display->display_options['sorts']['sid']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['sid']['expose']['label'] = 'Submission ID';
  /* Sort criterion: UW Endowment Submissions: Student number */
  $handler->display->display_options['sorts']['student_number']['id'] = 'student_number';
  $handler->display->display_options['sorts']['student_number']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['sorts']['student_number']['field'] = 'student_number';
  $handler->display->display_options['sorts']['student_number']['relationship'] = 'rsid';
  $handler->display->display_options['sorts']['student_number']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['student_number']['expose']['label'] = 'Student number';
  /* Sort criterion: UW Endowment Submissions: Submission update */
  $handler->display->display_options['sorts']['changed']['id'] = 'changed';
  $handler->display->display_options['sorts']['changed']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['sorts']['changed']['field'] = 'changed';
  $handler->display->display_options['sorts']['changed']['relationship'] = 'rsid';
  $handler->display->display_options['sorts']['changed']['order'] = 'DESC';
  $handler->display->display_options['sorts']['changed']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['changed']['expose']['label'] = 'Submission update';
  /* Filter criterion: UW Endowment Submissions: Refund code */
  $handler->display->display_options['filters']['code']['id'] = 'code';
  $handler->display->display_options['filters']['code']['table'] = 'uw_endowment_app_submissions_refunds';
  $handler->display->display_options['filters']['code']['field'] = 'code';
  $handler->display->display_options['filters']['code']['exposed'] = TRUE;
  $handler->display->display_options['filters']['code']['expose']['operator_id'] = 'code_op';
  $handler->display->display_options['filters']['code']['expose']['label'] = 'Refund code';
  $handler->display->display_options['filters']['code']['expose']['operator'] = 'code_op';
  $handler->display->display_options['filters']['code']['expose']['identifier'] = 'code';
  $handler->display->display_options['filters']['code']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['code']['group_info']['label'] = 'Refund code';
  $handler->display->display_options['filters']['code']['group_info']['identifier'] = 'code';
  $handler->display->display_options['filters']['code']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'AHS',
      'operator' => '=',
      'value' => 'AHS',
    ),
    2 => array(
      'title' => 'ACCTG',
      'operator' => '=',
      'value' => 'ACCTG',
    ),
    3 => array(
      'title' => 'ARTS',
      'operator' => '=',
      'value' => 'ARTS',
    ),
    4 => array(
      'title' => 'WEEF',
      'operator' => '=',
      'value' => 'WEEF',
    ),
    5 => array(
      'title' => 'WEEF_SE',
      'operator' => '=',
      'value' => 'WEEF_SE',
    ),
    6 => array(
      'title' => 'ENV',
      'operator' => '=',
      'value' => 'ENV',
    ),
    7 => array(
      'title' => 'GSEF',
      'operator' => '=',
      'value' => 'GSEF',
    ),
    8 => array(
      'title' => 'MATH',
      'operator' => '=',
      'value' => 'MATH',
    ),
    9 => array(
      'title' => 'MATH_SE',
      'operator' => '=',
      'value' => 'MATH_SE',
    ),
    10 => array(
      'title' => 'SCI',
      'operator' => '=',
      'value' => 'SCI',
    ),
    11 => array(
      'title' => 'SFD',
      'operator' => '=',
      'value' => 'SFD',
    ),
  );
  /* Filter criterion: UW Endowment Submissions: Academic Term */
  $handler->display->display_options['filters']['term']['id'] = 'term';
  $handler->display->display_options['filters']['term']['table'] = 'uw_endowment_app_submissions';
  $handler->display->display_options['filters']['term']['field'] = 'term';
  $handler->display->display_options['filters']['term']['relationship'] = 'rsid';
  $handler->display->display_options['filters']['term']['exposed'] = TRUE;
  $handler->display->display_options['filters']['term']['expose']['operator_id'] = 'term_op';
  $handler->display->display_options['filters']['term']['expose']['label'] = 'Academic Term';
  $handler->display->display_options['filters']['term']['expose']['operator'] = 'term_op';
  $handler->display->display_options['filters']['term']['expose']['identifier'] = 'term';
  $handler->display->display_options['filters']['term']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['term']['group_info']['label'] = 'Academic Term';
  $handler->display->display_options['filters']['term']['group_info']['identifier'] = 'term';
  $handler->display->display_options['filters']['term']['group_info']['group_items'] = array(
    1 => array(
      'title' => '',
      'operator' => '=',
      'value' => '',
    ),
    2 => array(
      'title' => '',
      'operator' => '=',
      'value' => '',
    ),
    3 => array(
      'title' => '',
      'operator' => '=',
      'value' => '',
    ),
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/endowment/data/export';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Exports';
  $handler->display->display_options['menu']['weight'] = '55';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $handler->display->display_options['tab_options']['weight'] = '0';

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['path'] = 'admin/endowment/data/export/csv_export';
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );
  $translatables['uw_endowment_app_submissions_export'] = array(
    t('Master'),
    t('Refunds Export'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('submission'),
    t('Student number'),
    t('.'),
    t('Name'),
    t('Receipt No'),
    t('Date'),
    t('User ID'),
    t('Email'),
    t('Academic Term'),
    t('Refund code'),
    t('Reasons'),
    t('Submission ID'),
    t('Submission update'),
    t('Page'),
    t('Data export'),
  );

  return $view;
}
